import os
import urllib

from models import Author, Greeting, Advert

from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.api import mail


import jinja2
import webapp2


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

# MAIN_PAGE_FOOTER_TEMPLATE = """\
#     <form action="/sign?%s" method="post">
#       <div><textarea name="content" rows="3" cols="60"></textarea></div>
#       <div><input type="submit" value="Sign Guestbook"></div>
#     </form>
#     <hr>
#     <form>Guestbook name:
#       <input value="%s" name="guestbook_name">
#       <input type="submit" value="switch">
#     </form>
#     <a href="%s">%s</a>
#   </body>
# </html>
# """

DEFAULT_GUESTBOOK_NAME = 'default_guestbook'
DEFAULT_ADVERT_NAME = 'default_advert'

# We set a parent key on the 'Greetings' to ensure that they are all
# in the same entity group. Queries across the single entity group
# will be consistent.  However, the write rate should be limited to
# ~1/second.

def guestbook_key(guestbook_name=DEFAULT_GUESTBOOK_NAME):
    """Constructs a Datastore key for a Guestbook entity.

    We use guestbook_name as the key.
    """
    return ndb.Key('Guestbook', guestbook_name)

def advert_key(advert_name=DEFAULT_ADVERT_NAME):
    """Constructs a Datastore key for a Advert entity.

    We use advert_name as the key.
    """
    return ndb.Key('Advert', advert_name)


# class Author(ndb.Model):
#     """Sub model for representing an author."""
#     identity = ndb.StringProperty(indexed=False)
#     email = ndb.StringProperty(indexed=False)
#
#
# class Greeting(ndb.Model):
#     """A main model for representing an individual Guestbook entry."""
#     author = ndb.StructuredProperty(Author)
#     content = ndb.StringProperty(indexed=False)
#     date = ndb.DateTimeProperty(auto_now_add=True)


class MainPage(webapp2.RequestHandler):

    def get(self):
        guestbook_name = self.request.get('guestbook_name',
                                          DEFAULT_GUESTBOOK_NAME)
        greetings_query = Greeting.query(
            ancestor=guestbook_key(guestbook_name)).order(-Greeting.date)
        greetings = greetings_query.fetch(10)

        advert_name = DEFAULT_ADVERT_NAME
        adverts_query = Advert.query(
            ancestor=advert_key(advert_name)).order(-Advert.date)
        adverts = adverts_query.fetch(10)

        user = users.get_current_user()
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user': user,
            'greetings': greetings,
            'adverts': adverts,
            'guestbook_name': urllib.quote_plus(guestbook_name),
            'url': url,
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))

class Guestbook(webapp2.RequestHandler):
    def post(self):
        # We set the same parent key on the 'Greeting' to ensure each
        # Greeting is in the same entity group. Queries across the
        # single entity group will be consistent. However, the write
        # rate to a single entity group should be limited to
        # ~1/second.
        guestbook_name = self.request.get('guestbook_name',
                                          DEFAULT_GUESTBOOK_NAME)
        greeting = Greeting(parent=guestbook_key(guestbook_name))

        if users.get_current_user():
            greeting.author = Author(
                    identity=users.get_current_user().user_id(),
                    email=users.get_current_user().email())

        greeting.content = self.request.get('content')
        greeting.put()

        query_params = {'guestbook_name': guestbook_name}
        self.redirect('/?' + urllib.urlencode(query_params))

class AddAdvert(webapp2.RequestHandler):
    def post(self):
        # We set the same parent key on the 'Greeting' to ensure each
        # Greeting is in the same entity group. Queries across the
        # single entity group will be consistent. However, the write
        # rate to a single entity group should be limited to
        # ~1/second.
        advert_name = self.request.get('advert_name',
                                           DEFAULT_ADVERT_NAME)
        advert = Advert(parent=advert_key(advert_name))

        if users.get_current_user():
            advert.author = Author(
                    identity=users.get_current_user().user_id(),
                    email=users.get_current_user().email())

        advert.content = self.request.get('advert-content')
        advert.title = self.request.get('advert-title')
        advert.put()

        query_params = {'guestbook_name': DEFAULT_GUESTBOOK_NAME}
        self.redirect('/?' + urllib.urlencode(query_params))

class Mail(webapp2.RequestHandler):
    def get(self):
        mail = self.request.get('mail');
        user = users.get_current_user()
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user': user,
            'mail': mail,
            'url': url,
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('mail.html')
        self.response.write(template.render(template_values))

    def post(self):
        # takes input from user
        user = users.get_current_user()
        if user:
            fromMail = user.email()
            toMail = self.request.get("toMail")
            subject = self.request.get("subject")
            content = self.request.get("content")
            message = mail.EmailMessage(sender=fromMail, subject=subject)

            message.to = toMail
            message.body = """
            Wiadomosc z serwisu ogloszeniowego
            Uzytkownik %s napisal:
                %s""" % (fromMail, content)
            message.send()
            # self.response.out.write("Wiadomosc zostala wyslana pomyslnie!")
            self.redirect('/')
        self.redirect('/mail')


class DeleteAdvert(webapp2.RequestHandler):
    def get(self):
        advertId = self.request.get("advertId")

        parent=advert_key(DEFAULT_ADVERT_NAME)
        # emp_k=Advert.get_by_key_name('default_advert')
        # emp = ndb.get(emp_k)
        # ndb.delete(emp_k)
        a = Advert.get_by_id(long(advertId), parent=parent)
        # a.delete()

        a.key.delete()
        #
        # advert_name = self.request.get('advert_name',
        #                                   DEFAULT_ADVERT_NAME)
        # adverts_query = Advert.query(
        #     ancestor=guestbook_key(advert_name))
        # adverts = adverts_query.fetch(1)
        self.redirect('/')

class EditAdvert(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        advertId = self.request.get("advertId")
        advert_name = self.request.get('advert_name',
                                           DEFAULT_ADVERT_NAME)

        parent=advert_key(DEFAULT_ADVERT_NAME)
        advert = Advert(parent=advert_key(advert_name))
        a = advert.get_by_id(long(advertId),parent=parent)
        template_values = {
            'user': user,
            'advert' : a,
        }

        template = JINJA_ENVIRONMENT.get_template('edit.html')
        self.response.write(template.render(template_values))

    def post(self):
        if id > 0:
            advertId = self.request.get("advertId")

            parent=advert_key(DEFAULT_ADVERT_NAME)
            advert = Advert(parent=advert_key(DEFAULT_ADVERT_NAME))
            adv = advert.get_by_id(long(advertId),parent=parent)

            adv.title = self.request.get('title')
            adv.content = self.request.get('content')

            adv.put()
            self.redirect('/')

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/sign', Guestbook),
    ('/addAdvert', AddAdvert),
    ('/mail', Mail),
    ('/deleteAdvert', DeleteAdvert),
    ('/editAdvert', EditAdvert),
], debug=True)